# artui - svelte components for sveltekit/directus

#### Experimental

It's dependency free but will need a bit of manual wiring once its deployed in a project.

Will try to keep it very flexible but will have some defaults in regards to how both svelte and directus operate.

Mainly for personal use but feel free to use and contribute