/**
 * Copies a value to the clipboard as text and handles success/error callbacks.
 * @async
 * @param {unknown} x - The value to copy to clipboard (will be stringified)
 * @param {function(): (void|Promise<void>)} [success] - Optional success callback
 * @param {function([unknown]): (void|Promise<void>)} [error] - Optional error callback
 * @returns {Promise<void>} Promise that resolves when clipboard operation completes
 * @example
 * await copyToClipboard(42, () => console.log('Copied!'), err => console.error('Failed:', err));
 */

export async function copyToClipboard(x, success, error) {
	navigator.clipboard
		.writeText(String(x))
		.then(() => {
			if (success) success()
		})
		.catch((err) => {
			if (error) error(err)
		})
}
