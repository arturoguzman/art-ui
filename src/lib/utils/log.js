/**
 * @param {string} searchParams - URL search params of the request
 */

import { clearEmptyValues, objectToString } from './object.js'

const getSearchParams = (searchParams) => {
	searchParams = searchParams.slice(1)
	return Object.fromEntries(new URLSearchParams(searchParams))
}

/**
 * @param {URL} url - URL of the request
 */

const parseUrl = (url) => {
	const params = Object.entries(getSearchParams(url.search))
	return {
		path: url.pathname,
		params: url.search.replaceAll('?', '').replaceAll('&', ' ')
	}
}

/**
 * @param {string|Record<any,any>|number|null|undefined|any[]} message
 *
 */
const parseMessage = (message) => {
	if (typeof message === 'string' || typeof 'message' === 'number') {
		return message
	}
	if (Array.isArray(message)) {
		return 'it is an array'
	}
	if (message === undefined || message === null) {
		return ''
	}
	// if(message){
	//   message.toString()
	// }
	return message.toString()
}

/**
 * Log to the console with style
 * @param {number|{status: number, content?: string|object, style?:'info'|'error'|'warn'|string, event?: import("@sveltejs/kit").RequestEvent, response?: Response, locals?: App.Locals, send?: function}} arg1 - Status code or object with status, content and style.
 * @param {string|object|number|boolean|string[]|number[]} [content] - Message (optional)
 * @param {import("@sveltejs/kit").RequestEvent} [event] - Event (optional)
 * @param {Response} [response] - Response (optional)
 * @param {function} [send] - Function with result as object (optional)
 * @returns {function}
 */

export const log = (arg1, content, event, response, send) => {
	try {
		let _status = arg1
		let _event = event
		let _content = content
		let _response = response
		let _fn = send
		let error = false
		let style = undefined
		let url = undefined
		let ip = undefined
		let method = undefined
		let ms = undefined
		let message = undefined
		if (typeof arg1 === 'object') {
			_status = arg1.status
			_event = arg1.event
			_content = arg1.content
			_fn = arg1.send
		}
		if (_response) {
			_status = response?.status ?? _status
		}
		if (_event) {
			method = _event.request.method
			url = parseUrl(_event.url)
			ip = _event.request.headers.get('x-real-ip') || _event?.getClientAddress() || ''
			message = _event.locals.error
			_status = _event.locals.status ?? _status
			ms = Date.now() - _event.locals?.startTimer
		}
		if (typeof _status === 'number' && _status >= 300 && _status <= 399) {
			style = 'warn'
		}
		if (typeof _status === 'number' && _status >= 400) {
			error = true
			style = 'error'
		}
		const toReturn = {
			timestamp: new Date().toLocaleDateString('en-GB', {
				day: '2-digit',
				month: 'long',
				year: 'numeric',
				hour: '2-digit',
				minute: '2-digit',
				second: '2-digit',
				timeZone: 'UTC'
			}),
			method,
			code: _status,
			ip,
			...url,
			ms,
			spacing: '|',
			status: statusCodes[_status],
			content: _content,
			message: message
		}
		const result = Object.entries(clearEmptyValues(toReturn)).reduce((acc, ent) => {
			if (ent[1]) {
				acc += `${ent[1]} `
			}
			return acc
		}, '')
		if (style) {
			if (style === 'error') {
				console.log(`\x1b[31m${result}\x1b[0m`)
			} else if (style === 'warn') {
				console.log(`\x1b[33m${result}\x1b[0m`)
			} else if (style === 'info') {
				console.log(`\x1b[96m${result}\x1b[0m`)
			} else {
				console.log(`%c${result}`, style)
			}
		} else {
			console.log(result)
		}
		if (typeof _fn === 'function') {
			const a = _fn(clearEmptyValues(toReturn))
			return a
		}
		return () => clearEmptyValues(toReturn)
	} catch (error) {
		console.log('error in logger', error)
		return () => 'error in logger'
	}
}

const statusCodes = {
	100: 'Continue',
	200: 'Ok',
	201: 'Created',
	202: 'Accpeted',
	203: 'Non-Authorative Information',
	204: 'No Content',
	206: 'Partial Content',
	207: 'Multi-Status',
	208: 'Already Reported',
	226: 'IM Used',
	300: 'Multiple Choices',
	301: 'Moved Permanently',
	302: 'Found',
	303: 'See Other',
	304: 'Not Modified',
	305: 'Use Proxy',
	306: 'Switch Proxy',
	307: 'Temporary Redirect',
	308: 'Permanent Redirect',
	400: 'Bad Request',
	401: 'Unauthorised',
	402: 'Payment Required',
	403: 'Forbidden',
	404: 'Not Found',
	405: 'Method Not Allowed',
	406: 'Not Acceptable',
	407: 'Proxy Authentication Required',
	408: 'Request Timeout',
	409: 'Conflict',
	410: 'Gone',
	411: 'Length Required',
	412: 'Precondition Failed',
	413: 'Request Entity Too Large',
	414: 'Request-URI Too Long',
	416: 'Request Range Not Satisfiable',
	417: 'Expectation Failed',
	418: 'I am a teapot',
	420: 'Enchange Your Calm',
	422: 'Unprocessable Entity',
	423: 'Locked',
	424: 'Failed Dependency',
	425: 'Unordered Collection',
	426: 'Upgrade Required',
	429: 'Too Many Requests',
	432: 'Request Header Fields Too Large',
	444: 'No Response',
	451: 'Unavailable For Legal Reasons',
	494: 'Request Header Too Large',
	500: 'Internal Server Error',
	501: 'Not Implemented',
	502: 'Bad Gateway',
	503: 'Service Unavailable',
	504: 'Gateway Timeout',
	506: 'Variant Also Negotiates',
	507: 'Insufficient Storage',
	508: 'Loop Detected',
	509: 'Bandwidth Limit Exceeded',
	510: 'Not Extended'
}
