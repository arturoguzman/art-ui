import { objectToString } from "./object.js"

/**
 * Send notification to matrix
 * @param {string} endpoint - Where to send your message
 * @param {string} header - The first line of your notification
 * @param {string|number|object} content - The content of your message, either a string or an object
 * @param {string} [tag] - Optional, tag a user
 * @returns 
 */

export const notifyMatrix = async (
  endpoint,
  header,
  content,
  tag,
) => {
  let entries = ''
  if (typeof content === 'object') {
    entries = objectToString(content)
  } else {
    entries = content
  }
  const body = {
    html: `
    <h1>${header}</h1>
    ${entries}
    `,
    tag,
  }
  const message = await fetch(endpoint, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
  if (message.status === 202) {
    return 200
  } else {
    console.log('error sending to matrix')
    return 500
  }
}
