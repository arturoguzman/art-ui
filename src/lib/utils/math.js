/**
 * Returns the negative of the number passed
 * @param {number} number
 */
export const negative = (number) => {
	if (number > 0) return number * -1
	return number
}

/**
 * Returns the positive of the number passed
 * @param {number} number
 */
export const positive = (number) => {
	if (number < 0) return number * -1
	return number
}

/**
 * Returns the inverse of the number passed
 * @param {number} number
 */

export const inverse = (number) => {
	return number * -1
}

/**
 * Evaluates if any passed value is a number, this also includes strings that include numbers
 *
 * @param {unknown} x - Element to be evaluated
 * @returns {boolean}
 *
 */

export function isItANumber(x) {
	if (x === null) return false
	if (typeof x === 'boolean') return false
	if (typeof x === 'number') return true
	if (x === '') return false
	const n = Number(x)
	if (x !== '' && !Number.isNaN(n)) return true
	return false
}
