export function animateValue(initialValue, step, limit, duration) {
	let value = initialValue
	let startTime
	let endTime

	const easeIn = (t) => t * t // Ease-in function
	const easeOut = (t) => 1 - (1 - t) * (1 - t) // Ease-out function

	function updateValue(timestamp) {
		if (!startTime) {
			startTime = timestamp
			endTime = startTime + duration
		}

		const progress = Math.min(1, (timestamp - startTime) / duration)

		// Use ease-in for the first half and ease-out for the second half
		const easedProgress =
			progress < 0.5 ? easeIn(2 * progress) / 2 : 0.5 + easeOut(2 * (progress - 0.5)) / 2

		// Interpolate between initial and limit based on eased progress
		value = initialValue + (limit - initialValue) * easedProgress

		// Your logic here with the updated value

		// Check if animation is complete
		if (timestamp < endTime) {
			console.log(value)
			requestAnimationFrame(updateValue)
		} else {
			startTime = null
		}
	}

	// Trigger the animation by calling requestAnimationFrame
	requestAnimationFrame(updateValue)
}

// Example usage
