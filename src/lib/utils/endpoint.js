import { log } from './log.js'

/**
 *
 * Assets endpoint constructor
 *
 * @param {string} id - File id
 * @param {string} endpoint - Fetch endpoint where image is located
 * @param {fetch} fetchSvelte
 * @param {(string | undefined)} [params] - Url query params in string format
 * @param {(string | undefined)} [token]  - Access token
 * @param {(string | undefined)} [errorFile] - Alternative url for image in case endpoint fails
 */

export const assetEndpoint = (id, endpoint, fetchSvelte, params, token, errorFile) => {
	try {
		const url = `${endpoint.endsWith('/') ? endpoint.slice(0, -1) : endpoint}/${id}${params === '' ? '' : `?${params}`}`
		const headers = token ? { Authorization: `Bearer ${token}` } : {}
		return fetchSvelte(url, {
			method: 'GET',
			headers: headers
		})
	} catch (err) {
		log(err.status, err)
		return fetchSvelte(errorFile || '')
	}
}
