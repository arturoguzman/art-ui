/**
 * Convert an object into a hierarchical string in HTML
 * @param {any} o
 * @param {number} [inx]
 * @returns
 */

export const objectToString = (o, inx) => {
	let all = ''
	let i = inx ? inx : 2
	if (typeof o === 'object') {
		Object.entries(o).forEach((acc) => {
			if (typeof acc[1] === 'object' && acc[1]) {
				if (Array.isArray(acc[1])) {
					let section = ''
					acc[1].forEach((e) => {
						section += `${objectToString(e, i + 1)}</br>`
					})
					all += `<h${i}>${acc[0]}</h${i}>${section}`
				} else {
					all += `<h${i}>${acc[0]}</h${i}>${objectToString(acc[1], i + 1)}`
				}
			} else {
				all += `<p>${acc[0]}: ${acc[1]}</p>`
			}
		})
	} else {
		if (!o) {
			return all
		}
		all += `<p>${o}</p>`
	}
	return all
}

/**
 * Clear null and undefined values from an object
 * @param {Record<string|number,any>} data
 * @param {string[]} [remove]
 * @returns
 */

export const clearEmptyValues = (data, remove) => {
	for (const key in data) {
		if (
			data[key] === '' ||
			data[key] === null ||
			data[key] === undefined ||
			remove?.includes(key)
		) {
			delete data[key]
		}
	}
	return data
}
