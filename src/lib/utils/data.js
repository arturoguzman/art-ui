/**
 * Removes specified keys from an object.
 * @template T - The type of the input object.
 * @param {T extends Record<string, any>} obj - The object from which keys will be removed.
 * @param {string[]} keys - An array of keys to remove from the object.
 * @returns {Partial<T>} A new object with specified keys removed.
 */

export const removeKeys = (obj, keys) => {
	if (typeof obj === 'object' && obj !== null) {
		return Object.fromEntries(Object.entries(obj).filter(([key]) => !keys.includes(key)))
	}
	return obj
}

/**
 * Keeps only specified keys in an object.
 * @template T - The type of the input object.
 * @param {T extends Record<string, any>} obj - The object from which only specified keys will be kept.
 * @param {string[]} keys - An array of keys to keep in the object.
 * @returns {Partial<T>} A new object containing only specified keys.
 */

export const keepOnlyKeys = (obj, keys) => {
	if (typeof obj === 'object' && obj !== null) {
		return Object.fromEntries(Object.entries(obj).filter(([key]) => keys.includes(key)))
	}
	return obj
}

/**
 * Compares two arrays to determine if they contain the same elements with the same frequencies.
 *
 * @template T - The type of elements in the arrays.
 * @param {T[]} a - The first array to compare.
 * @param {T[]} b - The second array to compare.
 * @returns {boolean} - True if both arrays are equal, false otherwise.
 */

export function compareArrays(a, b) {
	if (a.length !== b.length) {
		return false
	}

	/**
	 * A map to store the frequency of each element in array 'a'.
	 */
	const map = new Map()

	// Increment the frequency of each element in 'a'
	for (const elem of a) {
		map.set(elem, (map.get(elem) || 0) + 1)
	}

	// Decrement the frequency of each element in 'b' and check if it exists in the map
	for (const elem of b) {
		if (!map.has(elem)) {
			return false
		}
		map.set(elem, map.get(elem) - 1)

		// If the frequency becomes negative, 'b' has more occurrences of this element than 'a'
		if (map.get(elem) < 0) {
			return false
		}
	}
	return true
}

/**
 * Prettify primitives and objects
 *
 * @param {unknown} x - Any element you'd like to export as JSON string
 * @returns {string}
 */

export function jstr(x) {
	return JSON.stringify(x, null, 2)
}

/**
 * Fuzzy search function
 * @param {string} search
 * @param {string} str
 * @returns {boolean}
 */

export function fuzzy(search, str) {
	search = String(search).toLowerCase()
	str = String(str).toLowerCase()
	let i = 0
	let n = -1
	let l = undefined
	for (; (l = search[i++]); ) if (!~(n = str.indexOf(l, n + 1))) return false
	return true
}

/**
 * Allows you to compare tho objects to evaluate if they are the same object.
 *
 * @param {Record<string|number, unknown>} obj1
 * @param {Record<string|number, unknown>} obj2
 * @returns {boolean}
 */

export function compareObjects(obj1, obj2) {
	const keys1 = Object.entries(obj1)
	const keys2 = Object.entries(obj2)
	if (keys1.length !== keys2.length) {
		return false
	}
	for (let key of keys1) {
		if (obj1[key[0]] !== obj2[key[0]]) {
			return false
		}
		if (obj2[key[1]] !== obj2[key[1]]) {
			return false
		}
	}
	return true
}
