/**
 * Get random ID, probably (very likely) not safe, so just use on UI components and non-sensitive id fields
 * @param {number} [length]
 * @returns {string}
 */

export function getId(length) {
	return `${([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
		(c ^ (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))).toString(length ?? 16)
	)}`
}

/**
 * Debounce function
 * @template {(...args: any[]) => any} T
 * @param {T} func The function to debounce.
 * @param {number} [timeout=300] The number of milliseconds to delay.
 * @returns {(...args: Parameters<T>) => void} Returns the new debounced function.
 */

export function debounce(func, timeout = 300) {
	let timer
	return (...args) => {
		clearTimeout(timer)
		timer = setTimeout(() => {
			func.apply(this, args)
		}, timeout)
	}
}

/**
 * @typedef {Object} ThrottleOptions
 * @property {boolean} [leading]
 * @property {boolean} [trailing]
 */

/**
 * Throttle function
 * @template {(...args: any[]) => any} T
 * @param {T} func
 * @param {number} wait
 * @param {ThrottleOptions} options
 * @returns {(...args: Parameters<T>) => void}
 */

export function throttle(func, wait, options) {
	let context
	let args
	let timeout = null
	let previous = 0
	const { leading = true, trailing = true } = options || {}

	const later = () => {
		previous = leading ? Date.now() : 0
		timeout = null
		func.apply(context, args)
		if (!timeout) context = args = null
	}

	const throttled = function (...throttledArgs) {
		const now = Date.now()
		if (!previous && !leading) previous = now
		const remaining = wait - (now - previous)
		context = this
		args = throttledArgs

		if (remaining <= 0 || remaining > wait) {
			if (timeout) {
				clearTimeout(timeout)
				timeout = null
			}
			previous = now
			func.apply(context, args)
			if (!timeout) context = args = null
		} else if (!timeout && trailing) {
			timeout = setTimeout(later, remaining)
		}
	}

	return throttled
}
