/**
 *@typedef {{background1: string; background2: string; background3: string; text1: string; text2: string; text3: string; accent1: string; accent2: string; accent3: string}} Colour
 */

/**
 * @typedef {{size: string; style: string; radius: string}} Border
 * /

/**
 * @typedef {{title: string[]; paragraph: string[]}} Font
 */

/**
 * @typedef {{version: string; language: string; dark:boolean; font: Font; colour: {dark: Colour, light: Colour}, border: Border}} Theme
 */

import { browser } from '$app/environment'
import { writable } from 'svelte/store'

/**
 * @typedef Theme
 */

export const theme = (() => {
	/**
	 * @type {import("svelte/store").Writable<Theme>}
	 */

	const { subscribe, set, update } = writable({
		version: '',
		language: '',
		dark: true,
		font: {
			title: [],
			paragraph: []
		},
		colour: {
			dark: {
				background1: '',
				background2: '',
				background3: '',
				text1: '',
				text2: '',
				text3: '',
				accent1: '',
				accent2: '',
				accent3: ''
			},
			light: {
				background1: '',
				background2: '',
				background3: '',
				text1: '',
				text2: '',
				text3: '',
				accent1: '',
				accent2: '',
				accent3: ''
			}
		},
		border: {
			size: '',
			style: '',
			radius: ''
		}
	})

	/**
	 * Evaluate if its necesary to reset the theme
	 * @param {string} key
	 * @param {Theme} theme
	 * @return {{data: Theme; reset: boolean}}
	 */

	function evalReset(key, theme) {
		if (browser) {
			const local = localStorage.getItem(key)
			if (!local) {
				return { data: theme, reset: true }
			}
			const data = JSON.parse(local)
			if (data.version !== theme.version) {
				return { data: data, reset: true }
			}
			return { data: data, reset: false }
		}
		return { data: theme, reset: true }
	}

	/**
	 * @param {Array<string, string|number|boolean|string[]|number[]|Record<string,any>|undefined|null>} keys
	 * @param {HTMLBodyElement} root
	 * @param {string|undefined} parent
	 */

	function parseThemeKeys(keys, root, parent) {
		if (!keys[1]) {
			return
		}
		if (Array.isArray(keys[1])) {
			root.style.setProperty(keys[0], keys[1].toString())
		} else if (typeof keys[1] === 'object') {
			const newParent = `${parent ? parent : ''}-${keys[0]}`
			Object.entries(keys[1]).forEach((_keys) => parseThemeKeys(_keys, root, newParent))
		} else {
			root.style.setProperty(`--theme${parent ? parent : ''}-${keys[0]}`, keys[1])
		}
	}

	/**
	 * @param {Theme} theme
	 * @param {boolean} mode
	 */

	function filterTheme(theme, mode) {
		const _theme = JSON.parse(JSON.stringify(theme))
		if (mode) {
			_theme.colour = _theme.colour.dark
			return _theme
		}
		_theme.colour = _theme.colour.light
		return _theme
	}

	/**
	 * @param {string} key
	 * @param {Theme} theme
	 */

	function setTheme(key, theme) {
		set(theme)
		if (browser) {
			const root = document.querySelector('body')
			const _theme = filterTheme(theme, theme.dark)
			Object.entries(_theme).forEach((pair) => parseThemeKeys(pair, root))
			localStorage.setItem(key, JSON.stringify(theme))
		}
	}

	/**
	 * Load the theme at layout, pass the key used to for the theme on localStorage,
	 * override the theme if reset or a theme object is passed
	 * @param {string} key
	 * @param {Theme} data
	 * @param {boolean|undefined} reset
	 */

	function load(key, data, reset) {
		let theme = { data: data, reset: false }
		if (reset) {
			setTheme(key, data)
			return
		}
		theme = evalReset(key, data)
		if (theme.reset) {
			setTheme(key, theme.data)
			readDark()
			return
		}
		setTheme(key, data)
		return
	}

	/**
	 * Reads user dark mode preference and sets it accordingly to the theme
	 */

	function readDark() {
		if (browser) {
			const prefersDark = window.matchMedia('(prefers-color-scheme: dark)')
			if (prefersDark.matches) {
				update((st) => {
					st.dark = true
					return st
				})
				return
			}
			update((st) => {
				st.dark = false
				return st
			})
			return
		}
	}

	/**
	 * Auto complete the fonts for css variables
	 * TODO: if font is sans, or serif, or mono set the appropiate fallbacks, rightnow its only sans for all
	 *
	 * @param {string} entry
	 * @param {number} index
	 *
	 */

	function fonts(entry, index) {
		let fonts = ''
		const unsub = subscribe((store) => {
			fonts = `${store.font[entry][index]}, system-ui, -apple-system, Helvetica Neue, Noto Sans, Liberation Sans, sans-serif, Apple Color Emoji`
		})
		unsub()
		return fonts
	}

	/**
	 * Function to toggle the theme between dark and light mode
	 * @param {string} key
	 */

	function toggleTheme(key) {
		let theme = undefined
		update((st) => {
			if (st.dark === true) {
				st.dark = false
			} else {
				st.dark = true
			}
			theme = st
			return st
		})
		if (theme) setTheme(key, theme)
	}
	return {
		subscribe,
		fonts,
		set,
		update,
		toggleTheme,
		load
	}
})()
