export type Picture = {
	charset?: string
	description?: string
	duration?: number
	embed?: string
	filename_disk?: string
	filename_download: string
	filesize?: number
	folder?: string
	height?: number
	id: string
	location?: string
	metadata?: unknown
	modified_by?: string
	modified_on: string
	storage: string
	tags?: unknown
	title?: string
	transformations?: unknown
	type?: string
	uploaded_by?: string
	uploaded_on: string
	width?: number
}
