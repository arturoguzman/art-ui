export type DirectusFile = {
	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	charset: string | null

	/**
	 * No description.
	 *
	 * Type in directus: text
	 * Type in database: text
	 */
	description: string | null

	/**
	 * No description.
	 *
	 * Type in directus: integer
	 * Type in database: integer
	 */
	duration: number | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	embed: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	filename_disk: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	filename_download: string

	/**
	 * No description.
	 *
	 * Type in directus: bigInteger
	 * Type in database: bigint
	 */
	filesize: number | null

	/**
	 * No description.
	 *
	 * Type in directus: uuid
	 * Type in database: uuid
	 */
	folder: DirectusFolder | DirectusFolder['id'] | null

	/**
	 * No description.
	 *
	 * Type in directus: integer
	 * Type in database: integer
	 */
	height: number | null

	/**
	 * No description.
	 *
	 * Type in directus: uuid
	 * Type in database: uuid
	 */
	id: string

	/**
	 * No description.
	 *
	 * Type in directus: text
	 * Type in database: text
	 */
	location: string | null

	/**
	 * No description.
	 *
	 * Type in directus: json
	 * Type in database: json
	 */
	metadata: any | null

	/**
	 * No description.
	 *
	 * Type in directus: uuid
	 * Type in database: uuid
	 */
	modified_by: DirectusUser | DirectusUser['id'] | null

	/**
	 * No description.
	 *
	 * Type in directus: timestamp
	 * Type in database: timestamp with time zone
	 */
	modified_on: string

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	storage: string

	/**
	 * No description.
	 *
	 * Type in directus: json
	 * Type in database: text
	 */
	tags: any | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	title: string | null

	/**
	 * No description.
	 *
	 * Type in directus: json
	 * Type in database: json
	 */
	transformations: any | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	type: string | null

	/**
	 * No description.
	 *
	 * Type in directus: uuid
	 * Type in database: uuid
	 */
	uploaded_by: DirectusUser | DirectusUser['id'] | null

	/**
	 * No description.
	 *
	 * Type in directus: timestamp
	 * Type in database: timestamp with time zone
	 */
	uploaded_on: string

	/**
	 * No description.
	 *
	 * Type in directus: integer
	 * Type in database: integer
	 */
	width: number | null
}

declare interface DirectusUser {
	/**
	 * No description.
	 *
	 * Type in directus: json
	 * Type in database: json
	 */
	auth_data: any | null

	/**
	 * No description.
	 *
	 * Type in directus: uuid
	 * Type in database: uuid
	 */
	avatar: DirectusFile | string | null

	/**
	 * No description.
	 *
	 * Type in directus: text
	 * Type in database: text
	 */
	description: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	email: string | null

	/**
	 * No description.
	 *
	 * Type in directus: boolean
	 * Type in database: boolean
	 */
	email_notifications: boolean | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	external_identifier: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	first_name: string | null

	/**
	 * No description.
	 *
	 * Type in directus: uuid
	 * Type in database: uuid
	 */
	id: string

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	language: string | null

	/**
	 * No description.
	 *
	 * Type in directus: timestamp
	 * Type in database: timestamp with time zone
	 */
	last_access: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	last_name: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	last_page: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	location: string | null

	/**
	 * No description.
	 *
	 * Type in directus: hash
	 * Type in database: character varying
	 */
	password: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	provider: string

	/**
	 * No description.
	 *
	 * Type in directus: uuid
	 * Type in database: uuid
	 */
	role: DirectusRole | DirectusRole['id'] | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	status: string

	/**
	 * No description.
	 *
	 * Type in directus: json
	 * Type in database: json
	 */
	tags: any | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	tfa_secret: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	theme: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	title: string | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	token: string | null
}

declare interface DirectusFolder {
	/**
	 * No description.
	 *
	 * Type in directus: uuid
	 * Type in database: uuid
	 */
	id: string

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	name: string

	/**
	 * No description.
	 *
	 * Type in directus: uuid
	 * Type in database: uuid
	 */
	parent: DirectusFolder | DirectusFolder['id'] | null
}

declare interface DirectusRole {
	/**
	 * No description.
	 *
	 * Type in directus: boolean
	 * Type in database: boolean
	 */
	admin_access: boolean

	/**
	 * No description.
	 *
	 * Type in directus: boolean
	 * Type in database: boolean
	 */
	app_access: boolean

	/**
	 * No description.
	 *
	 * Type in directus: text
	 * Type in database: text
	 */
	description: string | null

	/**
	 * No description.
	 *
	 * Type in directus: boolean
	 * Type in database: boolean
	 */
	enforce_tfa: boolean

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	icon: string

	/**
	 * No description.
	 *
	 * Type in directus: uuid
	 * Type in database: uuid
	 */
	id: string

	/**
	 * No description.
	 *
	 * Type in directus: csv
	 * Type in database: text
	 */
	ip_access: string[] | null

	/**
	 * No description.
	 *
	 * Type in directus: string
	 * Type in database: character varying
	 */
	name: string

	/**
	 * No description.
	 *
	 * Type in directus: alias
	 * Type in database: no column
	 */
	users: DirectusUser[] | null
}
