import { log } from '$lib/index.js'

export const handleError = async ({ error, event }) => {
	const errorId = crypto.randomUUID()
	event.locals.error = error
	//event.locals.error = error?.toString() || undefined
	//event.locals.errorStackTrace = error?.stack || undefined
	//event.locals.errorId = errorId
	//event.locals.errorIp = event.request.headers.get('x-real-ip') || event.getClientAddress()
	// console.log(error.toString())
	// log({
	// 	status: 500,
	// 	content: 'There has been an error',
	// 	event: event
	// })
	log({
		status: error.status ?? 500,
		event: event
	})
	return {
		message: 'An unexpected error occurred.',
		errorId
	}
}

/** @type {import('@sveltejs/kit').Handle} */

export async function handle({ event, resolve }) {
	if (event.url.pathname.startsWith('/custom')) {
		return new Response('custom response')
	}
	const response = await resolve(event)
	if (event.locals.error === undefined) {
		log({
			status: response.status,
			event: event
		})
	}
	return response
}
